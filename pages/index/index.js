Page({
	data: {
		startX: 0,
		moveX: 0
	},
	bindstart: function(e) {
		this.setData({
			startX: e.touches[0].clientX
		});
	},
	bindswipe: function (e) {
		// console.log('clientX:'+e.touches[0].clientX);
		// console.log('clientY:'+e.touches[0].clientY);

		// record currentX
		var clientX = e.touches[0].clientX;
		// minus X
		var moveX = clientX - this.data.startX;
		// set moveX for css
		this.setData({
			moveX: -moveX
		});
		console.log(moveX);
		// console.log(e.touches);
	}
})